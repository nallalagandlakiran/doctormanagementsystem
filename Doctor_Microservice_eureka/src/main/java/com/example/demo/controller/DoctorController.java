package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.model.Diagnosis;
import com.example.demo.model.Doctor;
import com.example.demo.model.Patient;
import com.example.demo.model.Patient_RestData;
import com.example.demo.repository.DoctorRepository;


@RestController
public class DoctorController {
	@Autowired
	DoctorRepository doctorRepo;
	
	@Autowired //injecting template from RestTemplate
	RestTemplate template;
    HttpHeaders headers = new HttpHeaders();
    
  
    HttpEntity <String> entity = new HttpEntity<String>(headers);
	//we can insert doctor details here...
	@PostMapping("/InsertDoctorDetails")
	public String insert(@RequestBody Doctor doctor)
	{
		doctorRepo.save(doctor);
		return "Successfully Inserted Doctor Details";
	}
	//we can retreive doctor Data here...
	@GetMapping("/retrieveData")
	public List<Doctor> retrieve()
	{
		return doctorRepo.findAll();
	}
	//retreiving doctor details by id...
	@PostMapping("/doctorById/{id}")
	public Doctor doctorData(@PathVariable("id") int id)
	{
		return doctorRepo.getById(id);
	}
	//retreiving diagnosis by id here...
	@GetMapping("/diagnosisById/{id}")
	public Diagnosis diagnosisData(@PathVariable("id") int id)
	{
		 return template.exchange("http://localhost:8082/GetdiagnosisById/" + id,
				 HttpMethod.GET,entity, Diagnosis.class).getBody();
	}
	//retreiving patient details by id..
	@GetMapping("/PatientById/{id}")
	public Patient patientData(@PathVariable("id") int id)
	{
		 return template.exchange("http://localhost:8081/GetPatientById/" + id,
				 HttpMethod.GET,entity, Patient.class).getBody();
	}
	//retreiving All details by id like : http://localhost:8083/doctor?idPatient=2&idDiagnosis=3&consultation=4
	@GetMapping("/doctor")
	public Patient_RestData patientDetails(@RequestParam int idPatient, @RequestParam int idDiagnosis, @RequestParam String consultation)
	{
		Patient_RestData  pdetails = new Patient_RestData();
		pdetails.setPatient(template.exchange("http://localhost:8081/GetPatientById/" + idPatient,
				 HttpMethod.GET,entity, Patient.class).getBody());
		pdetails.setDoctor(doctorRepo.getByconsultation(consultation));
		pdetails.setDiagnosis(template.exchange("http://localhost:8082/GetdiagnosisById/" + idDiagnosis,
				 HttpMethod.GET,entity, Diagnosis.class).getBody());
		return pdetails;
	}

}
