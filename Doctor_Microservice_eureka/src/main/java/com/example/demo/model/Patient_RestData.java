package com.example.demo.model;

public class Patient_RestData {
	private Patient patient;//to retreive patient details
	private Doctor doctor;//to retreive doctor details
	private Diagnosis diagnosis;//to retreive diagnosis details..
	
	//generating getters and setters
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	public Diagnosis getDiagnosis() {
		return diagnosis;
	}
	public void setDiagnosis(Diagnosis diagnosis) {
		this.diagnosis = diagnosis;
	}

}
