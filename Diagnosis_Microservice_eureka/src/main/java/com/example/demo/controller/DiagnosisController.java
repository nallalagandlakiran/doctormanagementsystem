package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Diagnosis;
import com.example.demo.repository.DiagnosisRepository;


@RestController
public class DiagnosisController {
	@Autowired// injecting from diagnosis repository
	DiagnosisRepository diagnosisRepo;
	//inserting the diagnosis details
	@PostMapping("/InsertData")
	public String insertData(@RequestBody Diagnosis diagnosis)
	{
		diagnosisRepo.save(diagnosis);
		return "Diagnosis Data Inserted Successfully";
	}
	//to retreive the data of diagnosis
	@GetMapping("/RetrieveData")
	public List<Diagnosis> retrieveData()
	{
		return diagnosisRepo.findAll();
	}
	//to retreive diagnosis by id
	@GetMapping("/GetdiagnosisById/{id}")
	public Diagnosis getData(@PathVariable("id") int id)
	{
		Diagnosis diag = diagnosisRepo.getById(id);
		System.out.println(diag.getName());
		return diag;
	}
	

}
