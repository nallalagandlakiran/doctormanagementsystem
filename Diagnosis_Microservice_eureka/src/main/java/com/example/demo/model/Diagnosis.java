package com.example.demo.model;
import javax.persistence.*;

@Entity
@Table(name ="Diagnosis")
public class Diagnosis {
	@Id//primary key
	@Column(name = "id")
	private int id;
	@Column(name = "name")
	private String name;
	@Column(name = "Description")
	private String description;
	//creating setters and getters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Diagnosis()
	{
		
	}
	public Diagnosis(int id, String name, String description) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
