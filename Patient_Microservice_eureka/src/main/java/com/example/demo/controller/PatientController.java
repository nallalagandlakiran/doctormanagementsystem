package com.example.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Patient;
import com.example.demo.repository.PatientRepository;

@RestController
public class PatientController {
	@Autowired
	PatientRepository patientRepo;
	
	@PostMapping("/InsertPatientData")
	public String insert(@RequestBody Patient patient)
	{
		patientRepo.save(patient);
		return "Patient Details Successfully Inserted";
	}
	
	@GetMapping("/PatientData")
	public List<Patient> retrieve()
	{
		List<Patient> patientList = patientRepo.findAll();
		return patientList;
		
	}
	
	@GetMapping("/GetPatientById/{id}")
	public Patient getByID(@PathVariable("id") Integer id)
	{
		return patientRepo.getReferenceById(id);
	}
}
